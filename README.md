# Professor Plum's Ravenous Researcher

Do you have what it takes to research with Professor Plum?

This challenge was focused on cookies. After navigating to /search.html and entering a location to search into the box, 
you were redirected to looking.php which if the location was correctly inputted as billiard room and the cookie Found_Boddy was set to 1
the flag was displayed.